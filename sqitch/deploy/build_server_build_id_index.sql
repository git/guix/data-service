-- Deploy guix-data-service:build_server_build_id_index to pg

BEGIN;

CREATE INDEX builds_build_server_build_id ON builds (build_server_build_id);

COMMIT;
