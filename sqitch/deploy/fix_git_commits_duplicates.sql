-- Deploy guix-data-service:fix_git_commits_duplicates to pg

BEGIN;

DELETE FROM git_commits WHERE id IN (
  SELECT DISTINCT b.id
  FROM git_commits AS a
  INNER JOIN git_commits AS b
  ON a.commit = b.commit AND
     a.git_branch_id = b.git_branch_id AND
     a.datetime = b.datetime AND
     a.id != b.id
  WHERE a.id < b.id
);

CREATE UNIQUE INDEX git_commits_unique_idx ON git_commits (git_branch_id, commit, datetime);

COMMIT;
