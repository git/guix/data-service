(define-module (tests model-lint-checker)
  #:use-module (srfi srfi-64)
  #:use-module (ice-9 match)
  #:use-module (guix-data-service database)
  #:use-module (guix-data-service model lint-checker))

(test-begin "test-model-lint-checker")

(with-postgresql-connection
 "test-model-lint-checker"
 (lambda (conn)
   (check-test-database! conn)

   (test-assert "single insert"
     (with-postgresql-transaction
      conn
      (lambda (conn)
        (define data
          `#((name-1
              #t
              ,(lint-checker-description-data->lint-checker-description-set-id
                conn
                '(("en_US" . "foo"))))
             (name-2
              #f
              ,(lint-checker-description-data->lint-checker-description-set-id
                conn
                '(("en_US" . "bar"))))))

        (match (lint-checkers->lint-checker-ids conn data)
          (#((? number? id1) (? number? id2))
           (match (lint-checkers->lint-checker-ids conn data)
             (#((? number? second-id1) (? number? second-id2))
              (and (= id1 second-id1)
                   (= id2 second-id2)))))))
      #:always-rollback? #t))))

(test-end)
