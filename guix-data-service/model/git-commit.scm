;;; Guix Data Service -- Information about Guix over time
;;; Copyright © 2019 Christopher Baines <mail@cbaines.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (guix-data-service model git-commit)
  #:use-module (ice-9 match)
  #:use-module (json)
  #:use-module (squee)
  #:use-module (srfi srfi-19)
  #:use-module (guix-data-service model utils)
  #:export (insert-git-commit-entry
            git-commit-exists?))

(define (insert-git-commit-entry conn
                                 git-branch-id
                                 commit
                                 datetime)
  (exec-query
   conn
   "
INSERT INTO git_commits (commit, git_branch_id, datetime)
VALUES ($1, $2, to_timestamp($3))
ON CONFLICT DO NOTHING"
   (list commit
         (number->string git-branch-id)
         (date->string datetime "~s"))))

(define (git-commit-exists? conn commit)
  (match (exec-query
          conn
          "SELECT 1 FROM git_commits WHERE commit = $1"
          (list commit))
    (() #f)
    (_ #t)))
