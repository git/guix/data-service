;;; guix-data-service -- Information about Guix over time
;;; Copyright © 2016, 2017, 2018 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2019 Christopher Baines <mail@cbaines.net>
;;;
;;; This file is part of guix-data-service.
;;;
;;; guix-data-service is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; guix-data-service is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with the guix-data-service.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Run the following command to enter a development environment for
;;; the guix-data-service:
;;;
;;;  $ guix environment -l guix-dev.scm

(use-modules ((guix licenses) #:prefix license:)
             (guix packages)
             (guix download)
             (guix git-download)
             (guix utils)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages databases)
             (gnu packages gnupg)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages package-management)
             (gnu packages pkg-config)
             (gnu packages texinfo)
             (gnu packages ruby)
             (srfi srfi-1))

(define guile-knots
  (let ((commit "c641c19ce42876b16186ea82f3803a56a43a5f91")
        (revision "1"))
    (package
    (name "guile-knots")
    (version (git-version "0" revision commit))
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.cbaines.net/git/guile/knots")
                    (commit commit)))
              (sha256
               (base32
                "1lnijs8cdifajy7n63v5cvhj5pd0abfxb0rj07mr67ihd4829d3d"))
              (file-name (string-append name "-" version "-checkout"))))
    (build-system gnu-build-system)
    (native-inputs
     (list pkg-config
           autoconf
           automake
           guile-3.0
           guile-lib
           guile-fibers))
    (inputs
     (list guile-3.0))
    (propagated-inputs
     (list guile-fibers))
    (home-page "https://git.cbaines.net/guile/knots")
    (synopsis "Patterns and functionality to use with Guile Fibers")
    (description
     "")
    (license license:gpl3+))))

(package
  (name "guix-data-service")
  (version "0.0.0")
  (source #f)
  (build-system gnu-build-system)
  (inputs
   (list guix
         guile-email
         guile-json-4
         guile-squee
         guile-fibers
         guile-knots
         guile-gcrypt
         guile-lzlib
         guile-readline
         guile-prometheus
         guile-next
         sqitch))
  (native-inputs
   (list autoconf
         automake
         ephemeralpg
         pkg-config))
  (synopsis "TODO")
  (description "TODO")
  (home-page "TODO")
  (license license:gpl3+))
