-- Deploy guix-data-service:git_repositories_query_substitutes to pg

BEGIN;

ALTER TABLE git_repositories
  ADD COLUMN query_substitutes boolean NOT NULL DEFAULT TRUE;

COMMIT;
